package com.ittechweb.enquete.services;

import com.ittechweb.enquete.models.Equipement;
import com.ittechweb.enquete.models.Participant;
import com.ittechweb.enquete.models.Proposition;
import com.ittechweb.enquete.models.Question;
import com.ittechweb.enquete.models.Reponse;
import com.ittechweb.enquete.models.Sondage;
import com.ittechweb.enquete.utils.GenericListResult;
import com.ittechweb.enquete.utils.GenericObjectResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by BM.SERVICES on 28/06/2017.
 */

public interface SynchParamService {

    @GET("sondage/lister")
    Call<GenericListResult<Sondage>> getSondages(@Query("userID") int id, @Query("limit") int limit, @Query("offset") int offset);

    @GET("question/lister")
    Call<GenericListResult<Question>> getQuestions(@Query("sondage") long idSondage, @Query("limit") int limit, @Query("offset") int offset);

    @GET("proposition/lister")
    Call<GenericListResult<Proposition>> getPropositions(@Query("question") long idQuestion, @Query("limit") int limit, @Query("offset") int offset);

    //@Headers("Content-Type: application/json")
    //@FormUrlEncoded
    //@POST("app/questionnaire/ajoutermobile")
    //Call<GenericObjectResult<Sondage>> ajouterMobile(@Field("sondage") long idSondage, @Field("reponses") List<Reponse> reponseList);

    //@Headers("Content-Type: application/json")
    @FormUrlEncoded
    @POST("questionnaire/ajoutermobile")
    Call<GenericObjectResult<Sondage>> ajouterMobile(@Field("participant") String participant);

    @FormUrlEncoded
    @POST("equipement/position")
    Call<GenericObjectResult<Equipement>> position(@Field("imei") String imei, @Field("latitude") double latitude, @Field("longitude") double longitude, @Field("batterie") double batterie);

    @FormUrlEncoded
    @POST("questionnaire/ajoutermobile")
    Call<GenericObjectResult<Participant>> synchronisationParticipation(@Field("participant") String participant, @Field("reponses") String reponses);
}
