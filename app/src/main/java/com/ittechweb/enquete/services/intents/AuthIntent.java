package com.ittechweb.enquete.services.intents;

import android.util.Log;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.events.AuthEvent;
import com.ittechweb.enquete.events.SynchroEvent;
import com.ittechweb.enquete.models.Sondage;
import com.ittechweb.enquete.models.User;
import com.ittechweb.enquete.services.AuthService;
import com.ittechweb.enquete.services.SynchParamService;
import com.ittechweb.enquete.utils.Config;
import com.ittechweb.enquete.utils.GenericListResult;
import com.ittechweb.enquete.utils.GenericObjectResult;
import com.ittechweb.enquete.utils.OttoBus;
import com.ittechweb.enquete.utils.Session;
import com.ittechweb.enquete.utils.Utils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.api.support.app.AbstractIntentService;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by BM.SERVICES on 25/05/2017.
 */
@EIntentService
public class AuthIntent extends AbstractIntentService {
    @Bean
    OttoBus bus;
    private Retrofit clientRetrofit;
    private AuthService authService;

    public AuthIntent() {
        super("AuthIntent");
    }

    @ServiceAction
    void loginAction(String login, String password) {
        //final User user = Utils.jsonStringToObject(utilisateur, User.class);
        //Create Webservice Client
        try {
            clientRetrofit = MainApplication.getRetrofit();
            authService = clientRetrofit.create(AuthService.class);
        }
        catch (Exception ex){
            return;
        }

        //Call Service
        Call<GenericObjectResult<User>> call = authService.connexion(MainApplication.getIMEI(), login, password);
        call.enqueue(new Callback<GenericObjectResult<User>>() {
            @Override
            public void onResponse(Call<GenericObjectResult<User>> call, Response<GenericObjectResult<User>> response) {
                Log.e(Config.TAG, "Msg: " + response.message());
                if(response.isSuccessful()){
                    Session<User> userSession = new Session<>(MainApplication.getInstance(), User.class);
                    userSession.clear();
                    if(response.body().getCode() == 1){
                        User user = response.body().getData();
                        user.save();
                        for (Sondage sondage : user.getSondages()){
                            sondage.getCategorie().save();
                            sondage.save();
                        }
                        userSession.add(user);
                    }
                    //Utils.toast(response.body().getMessage());
                    bus.post(new AuthEvent(response.body().getCode(), response.body().getMessage()));
                }
                else{
                    try {
                        Log.e(Config.TAG, response.message());
                        Log.e(Config.TAG, response.errorBody().string());
                    } catch (IOException e) {
                        Log.e(Config.TAG, e.getMessage());
                    }
                    //Utils.toast("Erreur survenue");
                    bus.post(new AuthEvent(-response.code(), "Erreur survenue.\nRéessayer plus tard"));
                }
            }

            @Override
            public void onFailure(Call<GenericObjectResult<User>> call, Throwable t) {
                bus.post(new AuthEvent(-100, "Impossible de joindre le serveur"));
                t.printStackTrace();
            }
        });
    }

    @ServiceAction
    void logoutAction(long userId) {
        //final User user = Utils.jsonStringToObject(utilisateur, User.class);
        //Create Webservice Client
        try {
            clientRetrofit = MainApplication.getRetrofit();
            authService = clientRetrofit.create(AuthService.class);
        }
        catch (Exception ex){
            return;
        }

        //Call Service
        Call<GenericObjectResult<User>> call = authService.deconnexion(userId);
        call.enqueue(new Callback<GenericObjectResult<User>>() {
            @Override
            public void onResponse(Call<GenericObjectResult<User>> call, Response<GenericObjectResult<User>> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 1 || response.code() == 200){
                        Session<User> userSession = new Session<>(MainApplication.getInstance(), User.class);
                        //userSession.getFirst().delete();
                        userSession.clear();
                    }
                    //Utils.toast(response.body().getMessage());
                    bus.post(new AuthEvent(response.body().getCode(), response.message()));
                }
                else{
                    bus.post(new AuthEvent(response.code(), "Erreur survenue.\nRéessayer plus tard"));
                }
            }

            @Override
            public void onFailure(Call<GenericObjectResult<User>> call, Throwable t) {
                bus.post(new AuthEvent(-100, "Impossible de joindre le serveur"));
                t.printStackTrace();
            }
        });
    }
}
