package com.ittechweb.enquete.services;

import com.ittechweb.enquete.models.Sondage;
import com.ittechweb.enquete.models.User;
import com.ittechweb.enquete.utils.GenericListResult;
import com.ittechweb.enquete.utils.GenericObjectResult;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by wilfried on 16/04/2017.
 */

public interface AuthService {
    @FormUrlEncoded
    @POST("auth/login")
    Call<GenericObjectResult<User>> connexion(@Field("imei") String imei, @Field("credential") String login, @Field("password") String password);

    @FormUrlEncoded
    @POST("auth/logout")
    Call<GenericObjectResult<User>> deconnexion(@Field("userId") long userId);
}
