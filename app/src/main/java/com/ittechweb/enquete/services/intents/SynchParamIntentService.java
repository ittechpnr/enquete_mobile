package com.ittechweb.enquete.services.intents;

import android.util.Log;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.events.SynchroEvent;
import com.ittechweb.enquete.models.Equipement;
import com.ittechweb.enquete.models.Participant;
import com.ittechweb.enquete.models.Proposition;
import com.ittechweb.enquete.models.Question;
import com.ittechweb.enquete.models.Reponse;
import com.ittechweb.enquete.models.ReponseParticipant;
import com.ittechweb.enquete.models.Sondage;
import com.ittechweb.enquete.models.Synchronisation;
import com.ittechweb.enquete.models.TypeProposition;
import com.ittechweb.enquete.models.User;
import com.ittechweb.enquete.services.SynchParamService;
import com.ittechweb.enquete.utils.Config;
import com.ittechweb.enquete.utils.GenericListResult;
import com.ittechweb.enquete.utils.GenericObjectResult;
import com.ittechweb.enquete.utils.OttoBus;
import com.ittechweb.enquete.utils.Session;
import com.ittechweb.enquete.utils.Utils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.api.support.app.AbstractIntentService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by BM.SERVICES on 25/05/2017.
 */
@EIntentService
public class SynchParamIntentService extends AbstractIntentService {
    @Bean
    OttoBus bus;
    Retrofit clientRetrofit;
    private SynchParamService synchParamService;

    public SynchParamIntentService() {
        super("SynchParamIntentService");
    }

    @ServiceAction
    void sondageAction(final int limit, final int offset) {
        //Create Webservice Client
        try {
            clientRetrofit = MainApplication.getRetrofit();
            synchParamService = clientRetrofit.create(SynchParamService.class);
        }
        catch (Exception ex){
            return;
        }

        Session<User> userSession = new Session<>(MainApplication.getInstance(), User.class);
        User user = userSession.getFirst();
        if(user != null){
            return;
        }

        //Call Service
        Call<GenericListResult<Sondage>> call = synchParamService.getSondages(user.getId(), limit, offset);
        call.enqueue(new Callback<GenericListResult<Sondage>>() {
            @Override
            public void onResponse(Call<GenericListResult<Sondage>> call, Response<GenericListResult<Sondage>> response) {
                if(response.isSuccessful()){
                    int nombreSondage = response.body().getRows().size();
                    int totalSondage = response.body().getTotal();
                    for (Sondage sondage : response.body().getRows()){
                        sondage.getCategorie().save();
                        sondage.save();
                        //Telecharger les questions du sondage
//                        questionAction(sondage.getId(), 10, 0);
//                        Log.e(Config.TAG, sondage.toString());
                    }
                    if(offset+nombreSondage == totalSondage){ //Fin chargement des sondages
                        bus.post(new SynchroEvent(Sondage.class.getSimpleName(), response.message()));
                    }
                    else{//Page suivante
                        sondageAction(limit, offset+limit);
                    }
                }
                else{
                    bus.post(new SynchroEvent(Sondage.class.getSimpleName(), "Erreur survenue.\nRéessayer plus tard"));
                }
            }

            @Override
            public void onFailure(Call<GenericListResult<Sondage>> call, Throwable t) {
                bus.post(new SynchroEvent(Sondage.class.getSimpleName(), "Impossible de joindre le serveur"));
                t.printStackTrace();
                Log.e(Config.TAG, "Error: " + t.getMessage());
            }
        });
    }

    @ServiceAction
    void questionAction(final long sondage, final int limit, final int offset) {
        //Create Webservice Client
        final Session<Synchronisation> synchronisationSession;
        try {
            clientRetrofit = MainApplication.getRetrofit();
            synchParamService = clientRetrofit.create(SynchParamService.class);
            synchronisationSession = new Session<Synchronisation>(MainApplication.getInstance(), Synchronisation.class);
        }
        catch (Exception ex){
            return;
        }

        //Call Service
        Call<GenericListResult<Question>> call = synchParamService.getQuestions(sondage, limit, offset);
        call.enqueue(new Callback<GenericListResult<Question>>() {
            @Override
            public void onResponse(Call<GenericListResult<Question>> call, Response<GenericListResult<Question>> response) {
                if(response.isSuccessful()){
                    int nombreQuestion = response.body().getRows().size();
                    int totalQuestion = response.body().getTotal();
                    Synchronisation synchronisation = synchronisationSession.getFirst();
                    if(synchronisation != null){
                        synchronisation.incrementSondage();
                        synchronisationSession.update(synchronisation);
                    }

                    for (Question question : response.body().getRows()){
                        question.getTypeQuestion().save();
                        if(question.getQuestion() != null){
                            //question.getQuestion().getTypeQuestion().save();
                            question.getQuestion().save();
                        }
                        //question.getSondage().save();
                        List<Proposition> propositions = question.getPropositions();
                        question.save();
                        Question quest = Question.getById(question.getId());
                        //Enregistrement des propositions
                        int i = 0;
                        for (Proposition proposition : propositions){
                            i++;
                            if(proposition.getTypeProposition() != null){
                                TypeProposition typeProposition = proposition.getTypeProposition();
                                typeProposition.save();
                                proposition.setTypeProposition(typeProposition);
                            }
                            proposition.setQuestion(quest);
                            proposition.save();
                            Log.e(Config.TAG + i, proposition.toString());
                        }
//                        Log.e(Config.TAG, "Propos: " + propositions.size());
                    }

                    if(nombreQuestion > 0){ //Page suivante
                        questionAction(sondage, limit, offset+limit);
                    }
                    else{ //Fin chargement des questions
                        bus.post(new SynchroEvent(Question.class.getSimpleName(), response.message()));
                    }
                }
                else{
                    bus.post(new SynchroEvent(Question.class.getSimpleName(), "Erreur survenue.\nRéessayer plus tard"));
                }
            }

            @Override
            public void onFailure(Call<GenericListResult<Question>> call, Throwable t) {
                bus.post(new SynchroEvent(Question.class.getSimpleName(), "Impossible de joindre le serveur"));
                t.printStackTrace();
                Log.e(Config.TAG, "Error: " + t.getMessage());
            }
        });
    }

    @ServiceAction
    void positionAction(String imei, double latitude, double longitude, double batterie) {
        //Create Webservice Client
        try {
            clientRetrofit = MainApplication.getRetrofit();
            synchParamService = clientRetrofit.create(SynchParamService.class);
        }
        catch (Exception ex){
            return;
        }

        //Call Service
        Call<GenericObjectResult<Equipement>> call = synchParamService.position(imei, latitude, longitude, batterie);
        call.enqueue(new Callback<GenericObjectResult<Equipement>>() {
            @Override
            public void onResponse(Call<GenericObjectResult<Equipement>> call, Response<GenericObjectResult<Equipement>> response) {
                if(response.isSuccessful() && response.body().getCode() > 0){
                    try{
                        Equipement equipement = response.body().getData();
                        equipement.save();
                        Log.e(Config.TAG, "Equipement: " + equipement.toString());
                    }
                    catch (Exception ex){
                        Log.e(Config.TAG, "Erreur position: " + ex.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GenericObjectResult<Equipement>> call, Throwable t) {
                //bus.post(new SynchroEvent(Question.class.getSimpleName(), "Impossible de joindre le serveur"));
                t.printStackTrace();
                Log.e(Config.TAG, "Error: " + t.getMessage());
            }
        });
    }

    @ServiceAction
    void propositionAction(final long question, final int limit, final int offset) {
        //Create Webservice Client
        try {
            clientRetrofit = MainApplication.getRetrofit();
            synchParamService = clientRetrofit.create(SynchParamService.class);
        }
        catch (Exception ex){
            return;
        }

        //Call Service
        Call<GenericListResult<Proposition>> call = synchParamService.getPropositions(question, limit, offset);
        call.enqueue(new Callback<GenericListResult<Proposition>>() {
            @Override
            public void onResponse(Call<GenericListResult<Proposition>> call, Response<GenericListResult<Proposition>> response) {
                if(response.isSuccessful()){
                    int nombreProposition = response.body().getRows().size();
                    int totalProposition = response.body().getTotal();
                    for (Proposition proposition : response.body().getRows()){
                        proposition.getQuestion().save();
                        if (proposition.getTypeProposition() != null){
                            proposition.getTypeProposition().save();
                        }
                        proposition.save();
                        Log.e(Config.TAG, proposition.toString());
                    }
                    if(offset+nombreProposition == totalProposition){ //Fin chargement des propositions de reponse
                        bus.post(new SynchroEvent(Proposition.class.getSimpleName(), response.message()));
                    }
                    else{//Page suivante
                        propositionAction(question, limit, offset+limit);
                    }
                    bus.post(new SynchroEvent(Proposition.class.getSimpleName(), response.message()));
                }
                else{
                    bus.post(new SynchroEvent(Proposition.class.getSimpleName(), "Erreur survenue.\nRéessayer plus tard"));
                }
            }

            @Override
            public void onFailure(Call<GenericListResult<Proposition>> call, Throwable t) {
                bus.post(new SynchroEvent(Proposition.class.getSimpleName(), "Impossible de joindre le serveur"));
                t.printStackTrace();
                Log.e(Config.TAG, "Error: " + t.getMessage());
            }
        });
    }

    @ServiceAction
    void ajouterMobileAction(final String jsonParticipant) {
        //Create Webservice Client
        try {
            clientRetrofit = MainApplication.getRetrofit();
            synchParamService = clientRetrofit.create(SynchParamService.class);
        }
        catch (Exception ex){
            bus.post(new SynchroEvent(Reponse.class.getSimpleName(), "Erreur de synchronisation."));
            return;
        }

        //Call Service
        Call<GenericObjectResult<Sondage>> call = synchParamService.ajouterMobile(jsonParticipant);
        call.enqueue(new Callback<GenericObjectResult<Sondage>>() {
            @Override
            public void onResponse(Call<GenericObjectResult<Sondage>> call, Response<GenericObjectResult<Sondage>> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 1){
                        Participant participant = Utils.jsonStringToObject(jsonParticipant, Participant.class);
                        for (Reponse reponse : participant.getReponses()){
                            reponse.delete();
                        }
                        Participant mParticipant = Participant.getById(participant.getId());
                        mParticipant.setReponses(); //Affecte 50 lignes
                        if(mParticipant.getReponses().size() > 0){
                            List<Reponse> reponses = new ArrayList<>();
                            for (Reponse reponse : mParticipant.getReponses()){
                                reponse.setParticipant(null);
                                reponse.getProposition().setQuestion(null);
                                if(reponse.getColonne() != null){
                                    reponse.getColonne().setQuestion(null);
                                    if(reponse.getColonne().getTypeProposition() != null){
                                        reponse.getColonne().setTypeProposition(null);
                                    }
                                }
                                reponses.add(reponse);
                            }
                            mParticipant.setReponses(reponses);
                            ajouterMobileAction(Utils.toJsonString(mParticipant));
                        }
                        else{
                            mParticipant.setIdRemote((int) response.body().getTotal()); //ID Serveur
                            mParticipant.save();
                        }
                    }
                    //Log.e("mParticipant", "Message: " + response.body().getCode());
                    bus.post(new SynchroEvent(Reponse.class.getSimpleName(), response.body().getMessage()));
                }
                else{
                    bus.post(new SynchroEvent(Reponse.class.getSimpleName(), "Erreur survenue.\nRéessayer plus tard"));
                }
            }

            @Override
            public void onFailure(Call<GenericObjectResult<Sondage>> call, Throwable t) {
                bus.post(new SynchroEvent(Reponse.class.getSimpleName(), "Impossible de joindre le serveur"));
                t.printStackTrace();
                Log.e(Config.TAG, "getLocalizedMessage: " + t.getLocalizedMessage());
                Log.e(Config.TAG, "Error: " + t.getMessage());
            }
        });
    }

    @ServiceAction
    void synchronisationParticipation(final String jsonParticipant, int idParticipant) {
        //Create Webservice Client
        final Participant participant;
        final List<Reponse> reponseList;
        String reponsesString;
        try {
            clientRetrofit = MainApplication.getRetrofit();
            synchParamService = clientRetrofit.create(SynchParamService.class);
            participant = Utils.jsonStringToObject(jsonParticipant, Participant.class);
            reponseList = Reponse.findByParticipant(idParticipant);
            List<Reponse> reponsesToConvert = new ArrayList<>();
            for (Reponse reponse : reponseList){
                reponse.setParticipant(null);
                reponse.getProposition().setQuestion(null);
                if(reponse.getColonne() != null){
                    reponse.getColonne().setQuestion(null);
                    if(reponse.getColonne().getTypeProposition() != null){
                        reponse.getColonne().setTypeProposition(null);
                    }
                }
                reponsesToConvert.add(reponse);
            }
            ReponseParticipant reponseParticipant = new ReponseParticipant(reponsesToConvert);
            reponsesString = Utils.toJsonString(reponseParticipant);
        }
        catch (Exception ex){
            Utils.toast("Erreur de synchronisation.");
//            bus.post(new SynchroEvent(Reponse.class.getSimpleName(), "Erreur de synchronisation."));
            return;
        }

        if(reponseList.size() == 0){
            Utils.toast("Synchronisation terminée.");
//            bus.post(new SynchroEvent(Reponse.class.getSimpleName(), "Synchronisation terminée."));
            return;
        }

        //Call Service
        Call<GenericObjectResult<Participant>> call = synchParamService.synchronisationParticipation(jsonParticipant, reponsesString);
        call.enqueue(new Callback<GenericObjectResult<Participant>>() {
            @Override
            public void onResponse(Call<GenericObjectResult<Participant>> call, Response<GenericObjectResult<Participant>> response) {
                if(response.isSuccessful()){
                    if(response.body().getTotal() > 0){ //Total = idParticipant venant du serveur
                        participant.setIdRemote((int) response.body().getTotal());
                        for (Reponse reponse : reponseList){
                            reponse.delete();
                        }
                        participant.save();
                        //Synchronisation du prochain participant

                        List<Participant> participantList = Participant.findAllLocal();
                        for (Participant mParticipant : participantList){
                            List<Reponse> reponseList = Reponse.findByParticipant(mParticipant.getId());
                            if(reponseList.size() > 0){
                                synchronisationParticipation(Utils.toJsonString(mParticipant), mParticipant.getId());
                                break;
                            }
                        }

                        bus.post(new SynchroEvent(Reponse.class.getSimpleName(), response.body().getMessage(), participantList.size()));
                        return;
                    }
                    //Log.e("mParticipant", "Message: " + response.body().getMessage());
                    bus.post(new SynchroEvent(Reponse.class.getSimpleName(), response.body().getMessage()));
                }
                else{
                    bus.post(new SynchroEvent(Reponse.class.getSimpleName(), "Erreur survenue.\nRéessayer plus tard"));
                }
            }

            @Override
            public void onFailure(Call<GenericObjectResult<Participant>> call, Throwable t) {
                bus.post(new SynchroEvent(Reponse.class.getSimpleName(), "Impossible de joindre le serveur"));
                t.printStackTrace();
                Log.e(Config.TAG, "getLocalizedMessage: " + t.getLocalizedMessage());
                Log.e(Config.TAG, "Error: " + t.getMessage());
            }
        });
    }

    private boolean hasNextParticipant(int offset){
        Participant mParticipant = Participant.getOne(offset);
        if(mParticipant != null){
            List<Reponse> reponses = Reponse.findByParticipant(mParticipant.getId());
            if(reponses.size() > 0){
                synchronisationParticipation(Utils.toJsonString(mParticipant), mParticipant.getId());
                return true;
            }
            return hasNextParticipant(offset++);
        }
        else{
            return false;
        }
    }
}
