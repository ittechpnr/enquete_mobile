package com.ittechweb.enquete.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.R;
import com.ittechweb.enquete.models.AdresseIp;
import com.ittechweb.enquete.models.User;
import com.ittechweb.enquete.utils.Session;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_configuration)
public class ConfigurationActivity extends AppCompatActivity {

    private Session<AdresseIp> adresseIpSession;
    private AdresseIp adresseIp;

    @ViewById(R.id.adresse_serveur)
    EditText edtAdresseServeur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adresseIpSession = new Session<>(MainApplication.getInstance(), AdresseIp.class);
        adresseIp = adresseIpSession.getFirst();
    }

    @AfterViews
    void initView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(adresseIp != null){
            edtAdresseServeur.setText(adresseIp.getAdresse());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_configuration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_enregistrer_ip){
            adresseIpSession.clear();
            String adresseServeur = edtAdresseServeur.getText().toString().trim();
            if(!TextUtils.isEmpty(adresseServeur)){
                adresseIp = new AdresseIp(adresseServeur);
                adresseIpSession.add(adresseIp);
            }

            finish();
            return true;
        }
        if(id == android.R.id.home){
            adresseIpSession.clear();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
