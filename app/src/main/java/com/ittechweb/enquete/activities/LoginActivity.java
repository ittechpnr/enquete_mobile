package com.ittechweb.enquete.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.R;
import com.ittechweb.enquete.events.AdresseServeurEvent;
import com.ittechweb.enquete.events.AuthEvent;
import com.ittechweb.enquete.services.intents.AuthIntent_;
import com.ittechweb.enquete.utils.OttoBus;
import com.ittechweb.enquete.utils.Utils;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @ViewById(R.id.email)
    EditText email;

    @ViewById(R.id.password)
    EditText password;

    @AfterViews
    void initView() {

    }

    @Bean
    OttoBus bus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_connexion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_configuration) {
            ConfigurationActivity_.intent(LoginActivity.this).start();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public void onConnexion(AuthEvent authEvent) {
        progressDialog.dismiss();
        if(authEvent.getCode() > 0){ //Authentification réussie
            //Chargement des paramètres
            //SynchParamIntentService_.intent(MainApplication.getInstance()).sondageAction(100, 0).start();
            MainActivity_.intent(LoginActivity.this).extra("synchronisationQuestion", true).start();
            finish();
        }
        else{ //Authentification échouée
            Toast.makeText(LoginActivity.this, authEvent.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Subscribe
    public void onServeurError(AdresseServeurEvent serveurEvent) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        Utils.toast(serveurEvent.getMessage());
    }

    @Click(R.id.btn_connexion)
    void connexion(){
        if(!MainApplication.isNetworkConnected()){
            Toast.makeText(LoginActivity.this, getString(R.string.connexion_fail), Toast.LENGTH_LONG).show();
            return;
        }

        String txtEmail = email.getText().toString().trim();
        if(!Utils.isEmailValid(txtEmail)){
            email.setError(getString(R.string.error_message_email));
            email.requestFocus();
            return;
        }

        String txtPassword = password.getText().toString().trim();
        if(txtPassword.length() < 6){
            password.setError(getString(R.string.password_length));
            password.requestFocus();
            return;
        }

        //Hide keybord
        Utils.hideKeybord(LoginActivity.this);
        progressDialog = ProgressDialog.show(LoginActivity.this, null, "Authentification en cours...", false, false);

        AuthIntent_.intent(MainApplication.getInstance())
                .loginAction(txtEmail, txtPassword)
                .start();
    }
}
