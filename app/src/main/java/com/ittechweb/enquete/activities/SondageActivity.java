package com.ittechweb.enquete.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.R;
import com.ittechweb.enquete.adapters.QuestionAdapter;
import com.ittechweb.enquete.models.Sondage;
import com.ittechweb.enquete.utils.OttoBus;
import com.ittechweb.enquete.utils.Session;
import com.ittechweb.enquete.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_sondage)
public class SondageActivity extends AppCompatActivity {

    @App
    MainApplication mApplication;

    @Bean
    OttoBus bus;

    @Bean
    QuestionAdapter questionAdapter;

    @ViewById(R.id.lvQuestion)
    ListView questionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus.register(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_participation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_nouvelle_participation) {
            ParticipationActivity_.intent(SondageActivity.this).start();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @AfterViews
    void initView() {
        Session<Sondage> sondageSession = new Session<>(MainApplication.getInstance(), Sondage.class);
        Sondage sondage = sondageSession.getFirst();
        if (sondage != null){
            setTitle(sondage.getLibelle());
            questionAdapter.setQuestions(sondage.getId());
            questionList.setAdapter(questionAdapter);
        }
    }
}
