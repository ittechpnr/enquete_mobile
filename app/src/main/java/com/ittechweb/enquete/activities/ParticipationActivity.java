package com.ittechweb.enquete.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.R;
import com.ittechweb.enquete.events.AdresseServeurEvent;
import com.ittechweb.enquete.events.SynchroEvent;
import com.ittechweb.enquete.models.Participant;
import com.ittechweb.enquete.models.Proposition;
import com.ittechweb.enquete.models.Question;
import com.ittechweb.enquete.models.Reponse;
import com.ittechweb.enquete.models.Sondage;
import com.ittechweb.enquete.services.intents.SynchParamIntentService_;
import com.ittechweb.enquete.utils.Config;
import com.ittechweb.enquete.utils.OttoBus;
import com.ittechweb.enquete.utils.Session;
import com.ittechweb.enquete.utils.Utils;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import io.apptik.widget.MultiSlider;

@EActivity(R.layout.activity_participation)
public class ParticipationActivity extends AppCompatActivity implements Serializable {

    Sondage sondage;

    @Bean
    OttoBus bus;

    private String validationDate;
    private ProgressDialog progressDialog;

    @ViewById(R.id.layoutQuestion)
    LinearLayout layoutQuestion;

    @ViewById(R.id.nom_participant)
    EditText edtNomParticipant;

    @ViewById(R.id.age_participant)
    EditText edtAgeParticipant;

    @ViewById(R.id.email_participant)
    EditText edtEmailParticipant;

    @ViewById(R.id.contact_participant)
    EditText edtContactParticipant;

    @ViewById(R.id.radio_sexe)
    RadioGroup radioSexeParticipant;

    List<Question> questionList;
    List<Reponse> reponseList;
    //Liste des propositions de reponse
    Map<Integer, Map<Integer, String>> hashMapReponses; //idProposition, idColonne (Proposition), String
    //Liste temporaire des propositions de reponse
    Map<Integer, Integer> listReponsesTemp;
    //Reponse champ de saisie libre (text, date, textarea), Key = idQuestion
    Map<Integer, String> listReponsesString = new HashMap<>();
    //Reponse CheckBox matrice, Key = idQuestion
    Map<Integer, List<CheckBox>> matriceCheckBox = new HashMap<>();
    //Reponse EditText matrice, Key = idQuestion
    Map<Integer, List<EditText>> matriceEditText = new HashMap<>();

    Context mContext;
    private Map<Integer, Map<Integer, Integer>> matriceMapReponses;
    private Map<Integer, Integer> matriceReponsesTemp;
    private Map<Integer, Integer> mapRadioGroup = new HashMap<>(); //idQuestion, idProposition
    private Map<Integer, Integer> mapEvaluationGradueeSimple = new HashMap<>(); //idProposition, valeurChoisie || valeurMin
    private Map<Integer, Integer> mapEvaluationGradueeMultiple = new HashMap<>(); //idProposition, valeurMax : Combiner à mapEvaluationGradueeSimple
    private Map<Integer, Integer> ligneMatrice = new HashMap<>(); //idProposition, idColonne || 0 (Checbox)
    private Map<Integer, Integer> colonneMatrice = new HashMap<>(); //idQuestion, idProposition || indexColonne (negatif)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus.register(this);
        Session<Sondage> sondageSession = new Session<>(MainApplication.getInstance(), Sondage.class);
        sondage = sondageSession.getFirst();
        if (sondage != null){
            questionList = Question.findBySondage(sondage.getId());
            setTitle(sondage.getLibelle());
        }
        mContext = ParticipationActivity.this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_valider_participation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_enregistrer) {
            new AlertDialog.Builder(ParticipationActivity.this)
                    .setIcon(R.mipmap.ic_alert)
                    .setTitle("Enregistrement de l'enquête")
                    .setMessage("Etes-vous sûr de vouloir enregistrer ?")
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Enregistrer le formulaire
                            saveEnquete();
                        }
                    })
                    .setNegativeButton("Non", null).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveEnquete() {
        reponseList = new ArrayList<>();
        progressDialog = ProgressDialog.show(ParticipationActivity.this, null, "Enregistrement en cours ...", false, true);
        String nomParticipant = edtNomParticipant.getText().toString().trim();
        String ageParticipant = edtAgeParticipant.getText().toString().trim();
        String emailParticipant = edtEmailParticipant.getText().toString().trim();
        String contactParticipant = edtContactParticipant.getText().toString().trim();
        String sexeParticipant = radioSexeParticipant.getCheckedRadioButtonId() == R.id.sexe_masculin ? "M" : "F";
        if(TextUtils.isEmpty(nomParticipant)){
            nomParticipant = "Anonyme";
        }
        if(TextUtils.isEmpty(emailParticipant)){
            emailParticipant = null;
        }
        if(TextUtils.isEmpty(contactParticipant)){
            contactParticipant = null;
        }

        try {
            //Participant
            Participant participant = new Participant();
            participant.setNom(nomParticipant);
            participant.setEmail(emailParticipant);
            participant.setContact(contactParticipant);
            participant.setSexe(sexeParticipant);
            participant.setAge(!ageParticipant.isEmpty() ? Integer.valueOf(ageParticipant) : 0);
            participant.save();

            //Liste de Reponses
            //hashMapReponses
            Set setReponses = hashMapReponses.entrySet();
            Iterator iteratorReponses = setReponses.iterator();
            Reponse reponse;
            while (iteratorReponses.hasNext()){
                Map.Entry entryReponse = (Map.Entry) iteratorReponses.next();
                int idProposition = (int) entryReponse.getKey();
                HashMap mapReponses = (HashMap) entryReponse.getValue();
                Set setProposition = mapReponses.entrySet();
                Iterator iteratorProposition = setProposition.iterator();
                Proposition mProposition = Proposition.getById(idProposition);
                while (iteratorProposition.hasNext()){
                    Map.Entry entryProposition = (Map.Entry) iteratorProposition.next();
                    int idColonne = (int) entryProposition.getKey();
                    String saisieMatrice = (String) entryProposition.getValue();

                    if(!TextUtils.isEmpty(saisieMatrice)){
                        reponse = new Reponse();
                        reponse.setParticipant(participant);
                        reponse.setProposition(mProposition);
                        reponse.setColonne(Proposition.getById(idColonne));
                        if(reponse.getColonne().getTypeProposition().getType() == "text"){
                            reponse.setLibre(saisieMatrice);
                        }
                        reponse.save();
                        reponseList.add(reponse);
                    }
                }
            }

            //MatriceRadio et hierachie : Selection unique par ligne
            setReponses = ligneMatrice.entrySet();
            iteratorReponses = setReponses.iterator(); //Lines de propositions
            while (iteratorReponses.hasNext()){
                Map.Entry entryProposition = (Map.Entry) iteratorReponses.next();//Propositions
                int idProposition = (int) entryProposition.getKey();
                int idColonne = (int) entryProposition.getValue(); //idColonne ou indexColonne
                //int idColonne = colonneMatrice.get(idProposition);
                Proposition mProposition = Proposition.getById(idProposition);
                reponse = new Reponse();
                reponse.setParticipant(participant);
                reponse.setProposition(mProposition);
                if(idColonne > 0){ //Matrice
                    reponse.setColonne(Proposition.getById(idColonne));
                }
                else if(idColonne < 0){ //Hierachie
                    reponse.setColonneIndex(idColonne);
                } //else Checkbox
                reponse.setLibre("");
                reponse.save();
                reponseList.add(reponse);
            }

            //mapRadioGroup : radio (selection unique par question
            setReponses = mapRadioGroup.entrySet();
            iteratorReponses = setReponses.iterator(); //Propositions
            while (iteratorReponses.hasNext()){
                Map.Entry entryProposition = (Map.Entry) iteratorReponses.next();//Propositions
                //int idQuestion = (int) entryProposition.getKey();
                int idProposition = (int) entryProposition.getValue();
                Proposition mProposition = Proposition.getById(idProposition);
                reponse = new Reponse();
                reponse.setParticipant(participant);
                reponse.setProposition(mProposition);
                reponse.setLibre("");
                reponse.save();
                reponseList.add(reponse);
            }

            //EvaluationGraduee
            setReponses = mapEvaluationGradueeSimple.entrySet();
            iteratorReponses = setReponses.iterator(); //Propositions
            while (iteratorReponses.hasNext()){
                Map.Entry entryProposition = (Map.Entry) iteratorReponses.next();//Propositions
                int idProposition = (int) entryProposition.getKey();
                Proposition mProposition = Proposition.getById(idProposition);
                reponse = new Reponse();
                reponse.setParticipant(participant);
                reponse.setProposition(mProposition);
                reponse.setMin((int) entryProposition.getValue());
                if(mProposition.isChoixmultiple()){
                    reponse.setMax(mapEvaluationGradueeMultiple.get(idProposition));
                }
                reponse.setLibre("");
                reponse.save();
                reponseList.add(reponse);
            }

            //matriceMapReponses
            Set setReponsesMatrice = matriceMapReponses.entrySet();
            Iterator iteratorReponsesMatrice = setReponsesMatrice.iterator();
            while (iteratorReponsesMatrice.hasNext()){
                Map.Entry entryReponseMatrice = (Map.Entry) iteratorReponsesMatrice.next();
                int idQuestion = (int) entryReponseMatrice.getKey();
                HashMap mapReponses = (HashMap) entryReponseMatrice.getValue();
                Set setPropositionMatrice = mapReponses.entrySet();
                Iterator iteratorPropositionMatrice = setPropositionMatrice.iterator();
                while (iteratorPropositionMatrice.hasNext()){
                    Map.Entry entryProposition = (Map.Entry) iteratorPropositionMatrice.next();
                    int idPropositionLigne = (int) entryProposition.getKey();
                    int idPropositionColonne = (int) entryProposition.getValue();
                    reponse = new Reponse();
                    reponse.setParticipant(participant);
                    reponse.setProposition(Proposition.getById(idPropositionLigne));
                    reponse.setLibre("");
                    if(idPropositionColonne < 1){
                        reponse.setColonneIndex(Math.abs(idPropositionColonne));
                    }
                    else{
                        reponse.setColonne(Proposition.getById(idPropositionColonne));
                    }
                    reponse.save();
                    reponseList.add(reponse);
                }
            }

            //listReponsesString
            Set setSaisieLibre = listReponsesString.entrySet();
            Iterator iteratorSaisieLibre = setSaisieLibre.iterator();
            Proposition proposition;
            int propositionText = 0;
            while (iteratorSaisieLibre.hasNext()){
                Map.Entry entrySaisieLibre = (Map.Entry) iteratorSaisieLibre.next();
                Question question = Question.getById((int) entrySaisieLibre.getKey());
                if(question != null){
                    List<Proposition> propositions = Proposition.findByQuestion(question.getId());
                    if(propositions.size() > 0){
                        proposition = propositions.get(0);
                    }
                    else{
                        propositionText++;
                        proposition = new Proposition();
                        proposition.setId(-(participant.getId()+propositionText));
                        proposition.setLibelle(String.valueOf(question.getId()));
                        proposition.setQuestion(question);
                        proposition.save();
                    }

                    reponse = new Reponse();
                    reponse.setParticipant(participant);
                    reponse.setProposition(proposition);
                    reponse.setLibre((String) entrySaisieLibre.getValue());
                    reponse.save();
                    reponseList.add(reponse);
                }
            }
            if(MainApplication.isNetworkConnected() && reponseList.size() > 0){ //Envoi sur le serveur
                //participant.setReponses(reponseList);
                Log.e("Participant", Utils.toJsonString(participant));
                SynchParamIntentService_.intent(MainApplication.getInstance()).synchronisationParticipation(Utils.toJsonString(participant), participant.getId()).start();
            }
            else{
                progressDialog.dismiss();
                finish();
            }
        }
        catch (Exception ex){
            progressDialog.dismiss();
            ex.printStackTrace();
        }
    }

    @AfterViews
    void initView(){
        createForm(Question.findBySondage(sondage.getId()));
        Utils.hideKeybord(ParticipationActivity.this);
        hashMapReponses = new HashMap<>();
        matriceMapReponses = new HashMap<>();
    }

    @Subscribe
    public void onSondageSynch(SynchroEvent synchroEvent){
        Utils.toast(synchroEvent.getMessage());
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(TextUtils.equals(synchroEvent.getTable(), Reponse.class.getSimpleName())){
            finish();
        }
    }

    @Subscribe
    public void onServeurError(AdresseServeurEvent serveurEvent) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        Utils.toast(serveurEvent.getMessage());
    }

    private void createForm(List<Question> questions){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //Margins left, top, right, bottom
        layoutParams.setMargins(0, 20, 0, 10);

        TableRow.LayoutParams paramsRow = new TableRow.LayoutParams();
        paramsRow.setMargins(5, 5, 5, 5);

        TableRow.LayoutParams paramsRadioButton = new TableRow.LayoutParams();
        paramsRadioButton.setMargins(5, 5, 5, 5);

        TableLayout.LayoutParams layoutParamsMatrice = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //Margins left, top, right, bottom
        layoutParamsMatrice.setMargins(0, 20, 0, 10);
        HorizontalScrollView scrollView;
        LinearLayout.LayoutParams layoutParamsTab = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //Margins left, top, right, bottom
        layoutParamsTab.setMargins(0, 20, 0, 10);
        TableLayout tableLayout;
        TableRow tableRow;
        TextView tvEntete;

        for (final Question question : questions){
            LinearLayout linearLayout = new LinearLayout(mContext);
            //Layout Width and Height
            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setOrientation(LinearLayout.VERTICAL);

            //Question label
            TextView tvQuestionLabel = new TextView(mContext);
            tvQuestionLabel.setText(question.getLibelle());
            linearLayout.addView(tvQuestionLabel);
            List<Proposition> propositionList = Proposition.findByQuestion(question.getId());
            switch (question.getTypeQuestion().getType().trim().toLowerCase()){
                case "radio": //OK
                    RadioGroup radioGroup = new RadioGroup(mContext);
                    radioGroup.setOrientation(propositionList.size() < 3 ? LinearLayout.HORIZONTAL : LinearLayout.VERTICAL);
                    for (final Proposition proposition : propositionList){
                        RadioButton radioButton = new RadioButton(mContext);
                        radioButton.setId(proposition.getId());
                        radioButton.setText(proposition.getLibelle());
                        radioGroup.addView(radioButton);
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                                Proposition propo = Proposition.getById(checkedId);
                                mapRadioGroup.put(propo.getQuestion().getId(), checkedId);
                            }
                        });
                    }
                    linearLayout.addView(radioGroup);
                    break;
                case "input": //OK
                case "text": //OK
                    tvQuestionLabel.setVisibility(View.GONE);
                    TextInputLayout labelLayout = new TextInputLayout(mContext);
                    labelLayout.setLayoutParams(layoutParams);
                    final EditText editText = new EditText(mContext);
                    editText.setHint(question.getLibelle());
                    editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if(!hasFocus){
                                addPropositionSaisie(question.getId(), editText.getText().toString().trim());
                            }
                        }
                    });
                    labelLayout.addView(editText);
                    linearLayout.addView(labelLayout);
                    break;
                case "textarea": //OK
                    tvQuestionLabel.setVisibility(View.GONE);
                    TextInputLayout labelLayoutTextarea = new TextInputLayout(mContext);
                    labelLayoutTextarea.setLayoutParams(layoutParams);
                    final EditText textarea = new EditText(mContext);
                    textarea.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if(!hasFocus){
                                addPropositionSaisie(question.getId(), textarea.getText().toString().trim());
                            }
                        }
                    });
                    textarea.setHint(question.getLibelle());
                    labelLayoutTextarea.addView(textarea);
                    linearLayout.addView(labelLayoutTextarea);
                    break;
                case "date": //OK
                    TextInputLayout labelLayoutDate = new TextInputLayout(mContext);
                    labelLayoutDate.setLayoutParams(layoutParams);
                    final EditText editTextDate = new EditText(mContext);
                    editTextDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if(!hasFocus){
                                addPropositionSaisie(question.getId(), editTextDate.getText().toString().trim());
                            }
                        }
                    });
                    editTextDate.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL | InputType.TYPE_CLASS_DATETIME);
                    editTextDate.setFocusable(false);
                    editTextDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new DateTimeDialog(editTextDate);
                        }
                    });
                    labelLayoutDate.addView(editTextDate);
                    linearLayout.addView(labelLayoutDate);
                    break;
                case "checkbox": //OK
                    for (final Proposition proposition : propositionList){
                        CheckBox checkBox = new CheckBox(mContext);
                        checkBox.setText(proposition.getLibelle());
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(isChecked){
                                    ligneMatrice.put(proposition.getId(), 0);
                                }
                                else if(ligneMatrice.containsKey(proposition.getId())){
                                    ligneMatrice.remove(proposition.getId());
                                }
                            }
                        });
                        linearLayout.addView(checkBox);
                    }
                    break;
                case "illustrationradio": //OK
                    ImageView imageViewRadio = new ImageView(mContext);
                    linearLayout.addView(imageViewRadio);
                    if(!TextUtils.isEmpty(question.getLien())){
                        String lienIcon = question.getLien().startsWith("/") ? question.getLien().substring(1) : question.getLien();
                        Picasso.with(mContext).load(MainApplication.BASE_URL + lienIcon).error(R.mipmap.ic_launcher).resize(400, 300).centerInside().into(imageViewRadio);
                    }
                    RadioGroup radioGroupIllust = new RadioGroup(mContext);
                    radioGroupIllust.setOrientation(LinearLayout.VERTICAL);

                    for (final Proposition proposition : propositionList){
                        RadioButton radioButton = new RadioButton(mContext);
                        radioButton.setId(proposition.getId());
                        radioButton.setText(proposition.getLibelle());
                        radioGroupIllust.addView(radioButton);
                        radioGroupIllust.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                                Proposition propo = Proposition.getById(checkedId);
                                mapRadioGroup.put(propo.getQuestion().getId(), checkedId);
                            }
                        });
                    }
                    linearLayout.addView(radioGroupIllust);
                    break;
                case "illustrationcheckbox": //OK
                    ImageView imageView = new ImageView(mContext);
                    linearLayout.addView(imageView);
                    if(!TextUtils.isEmpty(question.getLien())){
                        String lienIcon = question.getLien().startsWith("/") ? question.getLien().substring(1) : question.getLien();
                        Picasso.with(mContext).load(MainApplication.BASE_URL + lienIcon).error(R.mipmap.ic_launcher).resize(400, 300).centerInside().into(imageView);
                        //Picasso.with(mContext).load(MainApplication.BASE_URL + lienIcon).error(R.mipmap.ic_launcher).resize(300, 300).centerInside().into(imageView);
                    }
                    for (final Proposition proposition : propositionList){
                        CheckBox checkBox = new CheckBox(mContext);
                        checkBox.setText(proposition.getLibelle());
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(isChecked){
                                    ligneMatrice.put(proposition.getId(), 0);
                                }
                                else if(ligneMatrice.containsKey(proposition.getId())){
                                    ligneMatrice.remove(proposition.getId());
                                }
                            }
                        });
                        linearLayout.addView(checkBox);
                    }
                    break;
                case "imagecheckbox": //OK
                    for (final Proposition proposition : propositionList){
                        LinearLayout layoutChecboxImage = new LinearLayout(mContext);
                        CheckBox checkBox = new CheckBox(mContext);
                        checkBox.setText("");
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(isChecked){
                                    ligneMatrice.put(proposition.getId(), 0);
                                }
                                else if(ligneMatrice.containsKey(proposition.getId())){
                                    ligneMatrice.remove(proposition.getId());
                                }
                            }
                        });
                        layoutChecboxImage.addView(checkBox);
                        ImageView imageViewCheckbox = new ImageView(mContext);
                        layoutChecboxImage.addView(imageViewCheckbox);
                        if(!TextUtils.isEmpty(proposition.getLibelle())){
                            String lienIcon = proposition.getLibelle().startsWith("/") ? proposition.getLibelle().substring(1) : proposition.getLibelle();
                            Picasso.with(mContext).load(MainApplication.BASE_URL + lienIcon).error(R.mipmap.ic_launcher).resize(400, 300).centerInside().into(imageViewCheckbox);
                        }
                        linearLayout.addView(layoutChecboxImage);
                    }
                    break;
                case "imageradio": //OK
                    RadioGroup radioGroupRadio = new RadioGroup(mContext);
                    radioGroupRadio.setOrientation(LinearLayout.HORIZONTAL);
                    LinearLayout layoutRadioImage = new LinearLayout(mContext);
                    layoutRadioImage.setOrientation(LinearLayout.VERTICAL);
                    //layoutRadioImage.setLayoutParams(layoutParams);
                    //
                    LinearLayout.LayoutParams layoutImageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    LinearLayout layoutImage = new LinearLayout(mContext);
                    layoutImage.setOrientation(LinearLayout.VERTICAL);
                    layoutImage.setLayoutParams(layoutImageParams);

                    LinearLayout layoutImageContent = new LinearLayout(mContext);
                    layoutImageContent.setLayoutParams(layoutParams);
                    layoutImageContent.setOrientation(LinearLayout.HORIZONTAL);
                    int i = 0;
                    layoutImageParams.setMargins(0, 60, 0, 0);
                    for (final Proposition proposition : propositionList){
                        String libelle = "Image " + ++i;
                        RadioButton radioButton = new RadioButton(mContext);
                        radioButton.setId(proposition.getId());
                        radioButton.setText(libelle);
                        radioButton.setMaxLines(proposition.getId()); //Line
                        radioButton.setHint(String.valueOf(question.getId())); //Colonne
                        radioButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toogleRadio(proposition.getQuestion().getId(), v.getId());
                            }
                        });

                        ImageView ivRadio = new ImageView(mContext);
                        ivRadio.setLayoutParams(layoutImageParams);
                        layoutImage.addView(ivRadio);
                        layoutImage.addView(radioButton);
                        if(!TextUtils.isEmpty(proposition.getLibelle())){
                            String lienIcon = proposition.getLibelle().startsWith("/") ? proposition.getLibelle().substring(1) : proposition.getLibelle();
                            Picasso.with(mContext).load(MainApplication.BASE_URL + lienIcon).error(R.mipmap.ic_launcher).resize(400, 300).centerInside().into(ivRadio);
                        }
                    }
                    layoutRadioImage.addView(layoutImage);
                    linearLayout.addView(layoutRadioImage);
                    break;
                case "matrice": //OK
                    scrollView = new HorizontalScrollView(mContext);
                    scrollView.setLayoutParams(layoutParams);

                    //Le tableau
                    tableLayout = new TableLayout(mContext);
                    tableLayout.setLayoutParams(layoutParamsTab);

                    //Ligne du tableau
                    //LinearLayout tableRow = new LinearLayout(mContext);
                    tableRow = new TableRow(mContext);
                    tableRow.setLayoutParams(layoutParamsMatrice);
                    //tableRow.setOrientation(LinearLayout.HORIZONTAL);

                    //Ajout des entetes de la matrice
                    tvEntete = new TextView(mContext);
                    tvEntete.setText("");
                    tableRow.addView(tvEntete); //Ajout de la colonne 0 à la ligne

                    String typeProposition = "";
                    List<Proposition> propositionsLignes = new ArrayList<>();
                    List<Integer> listColumnWidth = new ArrayList<>();
                    final List<Integer> idsColonnes = new ArrayList<>();
                    for(Proposition proposition : propositionList){
                        if (proposition.getTypeProposition() != null){ //Entete de matrice
                            //Log.e("Colonne", proposition.toString());
                            idsColonnes.add(proposition.getId());
                            tvEntete = new TextView(mContext);
                            tvEntete.setText(proposition.getLibelle());
                            tvEntete.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            tvEntete.setLayoutParams(paramsRow);
                            tvEntete.measure(0, 0);
                            tvEntete.setWidth(tvEntete.getMeasuredWidth() > 100 ? tvEntete.getMeasuredWidth() : 100);
                            tvEntete.measure(0, 0);
                            listColumnWidth.add(tvEntete.getMeasuredWidth());
                            tableRow.addView(tvEntete); //Ajout des colonnes à la ligne
                            if (typeProposition == ""){
                                typeProposition = proposition.getTypeProposition().getType();
                            }
                        }
                        else{//Ligne de matrice
                            propositionsLignes.add(proposition);
                        }
                    }

                    //Ajout des vues "Colonnes"
                    tableLayout.addView(tableRow);

                    //Ajout des lignes
                    int l = 0;
                    for (final Proposition proposition : propositionsLignes){
                        l++;
                        tableRow = new TableRow(mContext);
                        tableRow.setId(proposition.getId());
                        tableRow.setBackgroundColor(l%2 == 0 ? Color.parseColor("#f9f9f9") : Color.parseColor("#ffffff"));
                        TextView tvLigneLibelle = new TextView(mContext);
                        tvLigneLibelle.setText(proposition.getLibelle());
                        tvLigneLibelle.setLayoutParams(paramsRow);
                        tableRow.addView(tvLigneLibelle);

                        //Ajout des cellules
                        for (int ic = 0; ic < idsColonnes.size(); ic++){
                            //Row container
                            LinearLayout layoutContentRow = new LinearLayout(mContext);
                            layoutContentRow.setGravity(Gravity.CENTER);
                            layoutContentRow.setMinimumWidth(listColumnWidth.get(ic));
                            final int finalIc = ic;
                            switch (typeProposition){
                                case "radio": //OK
                                    final RadioButton radioButton = new RadioButton(mContext);
                                    radioButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            toogleRadioMatrice(question.getId(), proposition.getId(), idsColonnes.get(finalIc));
                                        }
                                    });
                                    radioButton.setMaxLines(idsColonnes.get(finalIc)); //Colonne
                                    layoutContentRow.addView(radioButton);
                                    tableRow.addView(layoutContentRow);
                                    break;
                                case "checkbox": //OK
                                    final CheckBox checkBox = new CheckBox(mContext);
                                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                        @Override
                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                            Map<Integer, String> celluleMatrice = hashMapReponses.get(proposition.getId());
                                            if(isChecked){
                                                if(celluleMatrice == null){
                                                    celluleMatrice = new HashMap<>();
                                                }
                                                celluleMatrice.put(idsColonnes.get(finalIc), String.valueOf(isChecked));
                                            }
                                            else{
                                                if(celluleMatrice.containsKey(idsColonnes.get(finalIc))){
                                                    celluleMatrice.remove(idsColonnes.get(finalIc));
                                                }
                                            }
                                            hashMapReponses.put(proposition.getId(), celluleMatrice);
                                        }
                                    });
                                    layoutContentRow.addView(checkBox);
                                    tableRow.addView(layoutContentRow);
                                    break;
                                default: //OK
                                    final EditText edt = new EditText(mContext);
                                    edt.setHint("Cliquer");
                                    edt.setMaxLines(proposition.getId()); //Line
                                    edt.setMaxEms(idsColonnes.get(finalIc)); //Colonne
                                    edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                        @Override
                                        public void onFocusChange(View v, boolean hasFocus) {
                                            Map<Integer, String> celluleMatrice = hashMapReponses.get(proposition.getId());
                                            if(!hasFocus){
                                                if(celluleMatrice == null){
                                                    celluleMatrice = new HashMap<>();
                                                }
                                                celluleMatrice.put(idsColonnes.get(finalIc), edt.getText().toString());
                                            }
                                        }
                                    });
                                    edt.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                    layoutContentRow.addView(edt);
                                    tableRow.addView(layoutContentRow);
                                    break;
                            }
                        }
                        tableLayout.addView(tableRow);
                    }

                    scrollView.addView(tableLayout);
                    linearLayout.addView(scrollView);
                    break;
                case "hierachie": //OK
                    scrollView = new HorizontalScrollView(mContext);
                    scrollView.setLayoutParams(layoutParams);

                    //Le tableau
                    tableLayout = new TableLayout(mContext);
                    tableLayout.setLayoutParams(layoutParamsTab);

                    //Ligne du tableau
                    tableRow = new TableRow(mContext);
                    tableRow.setLayoutParams(layoutParamsMatrice);

                    //Ajout des entetes de la matrice
                    tvEntete = new TextView(mContext);
                    tvEntete.setText("");
                    tableRow.addView(tvEntete); //Ajout de la colonne 0 à la ligne

                    List<Integer> listColumnWidths = new ArrayList<>();
                    for(int p = 0; p < propositionList.size(); p++){
                        tvEntete = new TextView(mContext);
                        tvEntete.setText("" + (p+1));
                        tvEntete.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        tvEntete.setLayoutParams(paramsRow);
                        tvEntete.measure(0, 0);
                        tvEntete.setWidth(tvEntete.getMeasuredWidth() > 100 ? tvEntete.getMeasuredWidth() : 100);
                        tvEntete.measure(0, 0);
                        listColumnWidths.add(tvEntete.getMeasuredWidth());
                        tableRow.addView(tvEntete); //Ajout des colonnes à la ligne
                    }

                    //Ajout des vues "Colonnes"
                    tableLayout.addView(tableRow);

                    //Ajout des lignes
                    int k = 0;
                    for (final Proposition proposition : propositionList){
                        k++;
                        tableRow = new TableRow(mContext);
                        tableRow.setId(proposition.getId());
                        tableRow.setBackgroundColor(k%2 == 0 ? Color.parseColor("#f9f9f9") : Color.parseColor("#ffffff"));
                        TextView tvLigneLibelle = new TextView(mContext);
                        tvLigneLibelle.setText(proposition.getLibelle());
                        tvLigneLibelle.setLayoutParams(paramsRow);
                        tableRow.addView(tvLigneLibelle);

                        //Ajout des cellules
                        for (int ic = 0; ic < propositionList.size(); ic++){
                            //Row container
                            LinearLayout layoutContentRow = new LinearLayout(mContext);
                            layoutContentRow.setGravity(Gravity.CENTER);
                            layoutContentRow.setMinimumWidth(listColumnWidths.get(ic));
                            final RadioButton radioButton = new RadioButton(mContext);
                            final int finalIc = ic+1;
                            radioButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    toogleRadioMatrice(question.getId(), proposition.getId(), -finalIc);
                                }
                            });
                            //radioButton.setMaxLines(proposition.getId()); //Line
                            radioButton.setMaxLines(-finalIc); //Colonne
                            //radioButton.setMaxEms(finalIc); //Colonne
                            layoutContentRow.addView(radioButton);
                            tableRow.addView(layoutContentRow);
                        }
                        tableLayout.addView(tableRow);
                    }

                    scrollView.addView(tableLayout);
                    linearLayout.addView(scrollView);
                    break;
                case "evaluationgraduee": //OK
                    for (final Proposition proposition : propositionList){
                        LinearLayout sliderDetails = new LinearLayout(mContext);
                        sliderDetails.setLayoutParams(layoutParamsMatrice);
                        sliderDetails.setOrientation(LinearLayout.HORIZONTAL);
                        TextView tvSliderLabel = new TextView(mContext);
                        sliderDetails.addView(tvSliderLabel);
                        final TextView tvSliderMin = new TextView(mContext);
                        sliderDetails.addView(tvSliderMin);
                        final TextView tvSliderMax = new TextView(mContext);
                        sliderDetails.addView(tvSliderMax);

                        MultiSlider multiSlider = new MultiSlider(mContext);
                        MultiSlider.Thumb thumb1 = multiSlider.new Thumb();
                        thumb1.setValue(proposition.getMin());
                        multiSlider.setId(proposition.getId());
                        multiSlider.setMin(proposition.getMin());
                        multiSlider.setMax(proposition.getMax());
                        multiSlider.setStep(proposition.getStep());
                        multiSlider.addThumb(thumb1);
                        tvSliderMin.setText(String.valueOf(proposition.getMin()));

                        if(proposition.isChoixmultiple()){
                            MultiSlider.Thumb thumb2 = multiSlider.new Thumb();
                            thumb2.setValue(proposition.getMax());
                            tvSliderMax.setText(" - " + String.valueOf(proposition.getMax()));
                            multiSlider.addThumb(thumb2);
                            multiSlider.setMin(proposition.getMin());
                            multiSlider.setNumberOfThumbs(2);
                            multiSlider.setOnThumbValueChangeListener(new MultiSlider.SimpleChangeListener() {
                                @Override
                                public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                                    if (thumbIndex == 0) {
                                        tvSliderMin.setText(String.valueOf(value));
                                        mapEvaluationGradueeSimple.put(proposition.getId(), value);
                                    } else {
                                        tvSliderMax.setText(" - " + String.valueOf(value));
                                        mapEvaluationGradueeMultiple.put(proposition.getId(), value);
                                    }
                                }
                            });
                        }
                        else{
                            multiSlider.setNumberOfThumbs(1);
                            multiSlider.setOnThumbValueChangeListener(new MultiSlider.SimpleChangeListener() {
                                @Override
                                public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                                    if (thumbIndex == 0) {
                                        tvSliderMin.setText(String.valueOf(value));
                                    } else {
                                        tvSliderMin.setText(String.valueOf(value));
                                    }
                                    mapEvaluationGradueeSimple.put(proposition.getId(), value);
                                }
                            });
                        }

                        linearLayout.addView(sliderDetails);
                        linearLayout.addView(multiSlider);
                        break;
                    }
                    break;
                default:
                    break;
            }
            layoutQuestion.addView(linearLayout);
        }
    }

    /**
     * Click sur bouton radio dans une matrice
     * @param propositionLigneId
     * @param propositionColonneId
     */
    private void toogleRadioMatrice(int idQuestion, int propositionLigneId, Integer propositionColonneId) {
        TableRow tableRow = (TableRow) findViewById(propositionLigneId);
        if(tableRow != null){
            RadioButton radioButtonClicked = null;
            for(int i = 0; i < tableRow.getChildCount(); i++){
                if(tableRow.getChildAt(i) instanceof LinearLayout){
                    LinearLayout layoutConteneur = (LinearLayout) tableRow.getChildAt(i);
                    RadioButton radioButton = (RadioButton) layoutConteneur.getChildAt(0);
                    //tableRow.getId() = idPropositionLigne; radioButton.getMaxLines() = idColonneLigne
                    if(tableRow.getId() == propositionLigneId && radioButton.getMaxLines() == propositionColonneId){
                        radioButtonClicked = radioButton;
                    }
                    addPropositionMatrice(idQuestion, propositionLigneId, propositionColonneId);
                    radioButton.setChecked(false);
                }
            }
            if(radioButtonClicked != null){
                removePropositionMatrice(idQuestion, propositionLigneId, propositionColonneId);
                radioButtonClicked.setChecked(true);
            }
        }
    }

    /**
     * Suppresion d'un élément de réponse
     * @param idQuestion
     * @param idProposition
     */
    void removePropositionReponse(int idQuestion, int idProposition){
//        if(listMapReponses.containsKey(idQuestion)){
//            listReponsesTemp = listMapReponses.get(idQuestion);
//            listReponsesTemp.remove(idProposition);
//            //listMapReponses.get(idQuestion).remove(idProposition);
//        }
    }

    /**
     *
     * @param idQuestion
     * @param idProposition
     * @param idColonne
     */
    void addPropositionMatrice(int idQuestion, int idProposition, int idColonne){
        ligneMatrice.put(idProposition, idColonne);
    }

    /**
     * @todo
     * @param idQuestion
     * @param idProposition
     * @param idColonne
     */
    void removePropositionMatrice(int idQuestion, int idProposition, int idColonne){
        if(matriceMapReponses.containsKey(idQuestion)){
            matriceReponsesTemp = matriceMapReponses.get(idQuestion);
            matriceReponsesTemp.remove(idProposition);
            //matriceMapReponses.get(idQuestion).remove(idProposition);
        }
    }

    /**
     * Clique sur un bouton radio dans le cas d'une selection unique (Illustration radio)
     * @param idQuestion
     * @param idProposition
     */
    void toogleRadio(int idQuestion, int idProposition){
        List<Proposition> propositionList = Proposition.findByQuestion(idQuestion);
        for(Proposition proposition : propositionList){
            try {
                RadioButton radioButton = findViewById(proposition.getId());
                if(radioButton != null){
                    radioButton.setChecked(false);
                    //removePropositionReponse(proposition.getQuestion().getId(), proposition.getId());
                }
            }
            catch (Exception ex){
                Log.e(Config.TAG, "Not find: " + proposition.getId());
            }
        }
        RadioButton radioButton = (RadioButton) findViewById(idProposition);
        if(radioButton != null){
            radioButton.setChecked(true);
            mapRadioGroup.put(idQuestion, idProposition);
        }
    }

    void addPropositionSaisie(int idQuestion, String txtSaisi){
        listReponsesString.put(idQuestion, txtSaisi);
    }

    public void setValidationDate(String validationDate) {
        this.validationDate = validationDate;
    }

    private class DateTimeDialog implements DatePickerDialog.OnDateSetListener {

        private static final String TIME_PATTERN = "dd-MM-yyyy";
        //private static final String FULL_TIME_PATTERN = "dd-MM-yyyy HH:mm:ss";

        private Calendar calendar;
        private SimpleDateFormat dateTimeFormat, fullDateTimeFormat;
        FragmentManager mFragmentManager;
        EditText mEdtDateTime;

        DateTimeDialog(EditText edtDateTime){
            //this.mFragmentManager = getFragmentManager();
            this.mEdtDateTime = edtDateTime;
            calendar = Calendar.getInstance();
            dateTimeFormat = new SimpleDateFormat(TIME_PATTERN, Locale.getDefault());
            //fullDateTimeFormat = new SimpleDateFormat(FULL_TIME_PATTERN, Locale.getDefault());
            DatePickerDialog datePickerDialog = new DatePickerDialog(ParticipationActivity.this, this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(year, month, dayOfMonth);
            mEdtDateTime.setText(dateTimeFormat.format(calendar.getTime()));
        }
    }
}
