package com.ittechweb.enquete.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.R;
import com.ittechweb.enquete.models.User;
import com.ittechweb.enquete.utils.Session;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Session<User> userSession = new Session<>(MainApplication.getInstance(), User.class);
        if (userSession.getFirst() != null){ //User connected
            MainActivity_.intent(SplashActivity.this).start();
        }
        else {
            LoginActivity_.intent(SplashActivity.this).start();
        }
        finish();
    }
}
