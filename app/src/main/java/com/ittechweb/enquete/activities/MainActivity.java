package com.ittechweb.enquete.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.R;
import com.ittechweb.enquete.adapters.SondageAdapter;
import com.ittechweb.enquete.events.AdresseServeurEvent;
import com.ittechweb.enquete.events.AuthEvent;
import com.ittechweb.enquete.events.DoSynchroEvent;
import com.ittechweb.enquete.events.SynchroEvent;
import com.ittechweb.enquete.models.AppTable;
import com.ittechweb.enquete.models.Participant;
import com.ittechweb.enquete.models.Question;
import com.ittechweb.enquete.models.Reponse;
import com.ittechweb.enquete.models.Sondage;
import com.ittechweb.enquete.models.Synchronisation;
import com.ittechweb.enquete.models.User;
import com.ittechweb.enquete.services.intents.AuthIntent_;
import com.ittechweb.enquete.services.intents.SynchParamIntentService_;
import com.ittechweb.enquete.utils.Config;
import com.ittechweb.enquete.utils.OttoBus;
import com.ittechweb.enquete.utils.Session;
import com.ittechweb.enquete.utils.Utils;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    @App
    MainApplication mApplication;

    @Bean
    OttoBus bus;

    @Bean
    SondageAdapter sondageAdapter;

    @ViewById(R.id.lvSondage)
    ListView sondageList;

    @ViewById(R.id.layoutQuestion)
    LinearLayout layoutQuestion;
    private ProgressDialog progressDialog;
    private Session<User> userSession;
    private Session<Synchronisation> synchronisationSession;
    private Thread mSplashThread;

    @Extra
    boolean synchronisationQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus.register(this);
        synchronisationSession = new Session<Synchronisation>(MainApplication.getInstance(), Synchronisation.class);
        userSession = new Session<User>(MainApplication.getInstance(), User.class);
        if (synchronisationQuestion){
            synchronisation();
        }
    }

    private void synchronisation(){
        User user = userSession.getFirst();
        if(user != null && user.getSondages().size() > 0){
            progressDialog = ProgressDialog.show(MainActivity.this, null, "Chargement des paramètres ...", false, true);
            Synchronisation synchronisation = new Synchronisation();
            for (Sondage sondage : user.getSondages()){
                synchronisation.incrementSondage();
                synchronisationSession.update(synchronisation);
                SynchParamIntentService_.intent(MainApplication.getInstance()).questionAction(sondage.getId(),5, 0).start();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userSession = new Session<>(MainApplication.getInstance(), User.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_deconnexion) {
            List<Reponse> reponses = Reponse.findAll();
            if(reponses.size() > 0){
                Utils.toast("Veuillez synchroniser toutes vos données avant d'effectuer cette opération");
                id = R.id.action_synchronisation; //Forcer la synchronisation
            } else if(!userSession.isEmpty()){ //User allways connected
                new AlertDialog.Builder(MainActivity.this)
                        .setIcon(R.mipmap.ic_alert)
                        .setTitle("Déconnexion")
                        .setMessage("Etes-vous sûr de vouloir vous déconnecter?")
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressDialog = ProgressDialog.show(MainActivity.this, null, "Déconnexion en cours...", false, true);

                                AuthIntent_.intent(MainApplication.getInstance())
                                        .logoutAction(userSession.getFirst().getId())
                                        .start();
                            }
                        })
                        .setNegativeButton("Non", null).show();
                return true;
            }
            else{
                return true;
            }

        }
        if (id == R.id.action_download) {
//            SynchParamIntentService_.intent(MainApplication.getInstance()).sondageAction(100, 0).start();
//            progressDialog = ProgressDialog.show(MainActivity.this,null, "Synchronisation des paramètres en cours...", false, true);
//            bus.post(new DoSynchroEvent(false));//Download params
            synchronisation();
            return true;
        }
        if (id == R.id.action_configuration) {
            ConfigurationActivity_.intent(MainActivity.this).start();
            return true;
        }
        if (id == R.id.action_synchronisation) {
            List<Participant> participants = Participant.findAllLocal();
            boolean synch = false;
            for (Participant participant : participants){
                List<Reponse> reponseList = Reponse.findByParticipant(participant.getId());
                if(reponseList.size() > 0){
                    synch = true;
                    SynchParamIntentService_.intent(MainApplication.getInstance()).synchronisationParticipation(Utils.toJsonString(participant), participant.getId()).start();
                    break;
                }
            }
            if(synch){
                progressDialog = ProgressDialog.show(MainActivity.this,null, "Sauvegarde en cours...", false, true);
                bus.post(new DoSynchroEvent(true));
                //SynchParamIntentService_.intent(MainApplication.getInstance()).synchronisationParticipation(Utils.toJsonString(participant), participant.getId()).start();
            }
            else{
                Utils.toast("Synchronisation complète.");
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @AfterViews
    void initView() {
        sondageList.setAdapter(sondageAdapter);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }
    }

    @ItemClick
    void lvSondageItemClicked(Sondage sondage) {
        Session<Sondage> sondageSession = new Session<>(MainApplication.getInstance(), Sondage.class);
        sondageSession.clear();
        sondageSession.add(sondage);
        SondageActivity_.intent(MainActivity.this).start();
    }

    @Subscribe
    public void onQuestionSynch(SynchroEvent synchroEvent){
        if(TextUtils.equals(synchroEvent.getTable(), Question.class.getSimpleName())){
            Synchronisation synchronisation = synchronisationSession.getFirst();
            if(synchronisation != null && synchronisation.getSondageRequest() == synchronisation.getSondageResponse()){
                progressDialog.dismiss();
            }
            progressDialog.dismiss();
        }
        else if(TextUtils.equals(synchroEvent.getTable(), Reponse.class.getSimpleName())){
            if(synchroEvent.getRestant() == 0 && progressDialog != null){
                progressDialog.dismiss();
            }
        }
        else if(progressDialog != null){
            progressDialog.dismiss();
        }
    }

    @Subscribe
    public void onServeurError(AdresseServeurEvent serveurEvent) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        Utils.toast(serveurEvent.getMessage());
    }

    @Subscribe
    public void onDeconnexion(AuthEvent logoutEvent) {
        progressDialog.dismiss();
        if(logoutEvent.getCode() > 0 && userSession.isEmpty()){ //Deconnexion réussie
            Log.e(Config.TAG, logoutEvent.getMessage());
            synchronisationSession.clear();
            userSession.clear();
            AppTable.deleteAll();
            startActivity(new Intent(MainActivity.this, SplashActivity.class));
            //SplashActivity.intent(MainApplication.getInstance()).start();
            finish();
        }
        else{ //Deconnexion échouée

        }
    }
}
