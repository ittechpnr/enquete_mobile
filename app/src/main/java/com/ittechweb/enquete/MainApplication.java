package com.ittechweb.enquete;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ittechweb.enquete.events.AdresseServeurEvent;
import com.ittechweb.enquete.events.AuthEvent;
import com.ittechweb.enquete.events.DoSynchroEvent;
import com.ittechweb.enquete.models.AdresseIp;
import com.ittechweb.enquete.utils.Config;
import com.ittechweb.enquete.utils.DateDeserializer;
import com.ittechweb.enquete.utils.OttoBus;
import com.ittechweb.enquete.utils.RequestInterceptor;
import com.ittechweb.enquete.utils.Session;
import com.ittechweb.enquete.utils.Utils;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by wilfried on 15/04/2017.
 */

@EApplication
public class MainApplication extends Application {

    @Bean
    OttoBus bus;
    private static Retrofit retrofit;
    public static String BASE_URL;
    public static String TOKEN;
    private static MainApplication sInstance;
    private Session<AdresseIp> adresseIpSession;

    public void onCreate() {
        super.onCreate();
        sInstance = this;
        bus.register(this);
        TOKEN = getResources().getString(R.string.token);
        adresseIpSession = new Session<>(sInstance, AdresseIp.class);
        if (adresseIpSession.getFirst() != null && adresseIpSession.getFirst().getAdresse().length() > 5) {
            String adresseServeur = adresseIpSession.getFirst().getAdresse();
            BASE_URL = adresseServeur.contains("http") ? adresseServeur : "http://" + adresseServeur;
        } else {
            BASE_URL = Config.MODE.equals("PROD") ? getResources().getString(R.string.prod_base_url) : getResources().getString(R.string.dev_base_url);
        }
        this.initComponent(); //Initialisation des composants et de la base de données
    }

    public synchronized static MainApplication getInstance() {
        return sInstance;
    }

    public synchronized static Retrofit getRetrofit() {
        if (retrofit == null) {
            try {
                OkHttpClient client = new OkHttpClient.Builder()
                        .connectTimeout(120, TimeUnit.SECONDS)
                        .writeTimeout(120, TimeUnit.SECONDS)
                        .readTimeout(120, TimeUnit.SECONDS)
                        .addInterceptor(new RequestInterceptor())
                        .build();

                Gson gson = new GsonBuilder()
                        .setDateFormat(Config.DATE_TIME_PATTERN)
                        .setDateFormat(Config.DATE_PATTERN)
                        .create();

                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();
            } catch (Exception ex) {
                //Utils.toast("Adresse serveur invalide");
                Bus mBus = new Bus();
                mBus.post(new AdresseServeurEvent(-1, "Adresse serveur invalide"));
                Log.e(Config.TAG, "Adresse serveur invalide");
                ex.printStackTrace();
            }

        }
        return retrofit;
    }

    @Background
    void initComponent() {
        FlowManager.init(new FlowConfig.Builder(MainApplication.getInstance()).build());
    }

    @Background
    @Subscribe
    public void doSynchro(DoSynchroEvent doSynchroEvent) {
        //@TODO
        if (doSynchroEvent.upload) {
            //ClientIntentService_.intent(MainApplication.getInstance().getApplicationContext()).uploadAction(doSynchroEvent.jsonClient).start();
        } else {
            //SynchroIntentService_.intent(MainApplication.getInstance().getApplicationContext()).prefixeAction().start();
        }
    }

    public static boolean isNetworkConnected() {
        Context context = MainApplication.getInstance().getApplicationContext();
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo infoNet = connectivity.getActiveNetworkInfo();
            if (infoNet != null) {
                if (infoNet.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getIMEI() {
        Context context = MainApplication.getInstance().getApplicationContext();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return "111111";
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return telephonyManager.getImei(0);
        }
        return telephonyManager.getDeviceId();
    }
}
