package com.ittechweb.enquete.adapters.itemview;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ittechweb.enquete.R;
import com.ittechweb.enquete.models.Question;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by BM.SERVICES on 04/07/2017.
 */
@EViewGroup(R.layout.item_question)
public class QuestionItemView extends LinearLayout {

    public QuestionItemView(Context context) {
        super(context);
    }

    @ViewById(R.id.tv_libelle)
    TextView tvLibelle;

    @ViewById(R.id.tv_type_question)
    TextView tvTypeQuestion;

    public void bind(Question question){
        tvLibelle.setText(question.getLibelle());
        tvTypeQuestion.setText(question.getTypeQuestion().getLibelle());
    }
}
