package com.ittechweb.enquete.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ittechweb.enquete.adapters.itemview.SondageItemView;
import com.ittechweb.enquete.adapters.itemview.SondageItemView_;
import com.ittechweb.enquete.models.Sondage;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

/**
 * Created by BM.SERVICES on 04/07/2017.
 */
@EBean
public class SondageAdapter extends BaseAdapter {

    List<Sondage> sondages;

    @RootContext
    Context context;

    @AfterInject
    void initAdapter(){
        sondages = Sondage.findAll();
    }

    @Override
    public int getCount() {
        return sondages.size();
    }

    @Override
    public Sondage getItem(int position) {
        return sondages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SondageItemView sondageItemView;
        if(convertView == null){
            sondageItemView = SondageItemView_.build(context);
        }
        else{
            sondageItemView = (SondageItemView) convertView;
        }

        sondageItemView.bind(getItem(position));

        return sondageItemView;
    }

    public void reload() {
        sondages = Sondage.findAll();
        notifyDataSetChanged();
    }
}
