package com.ittechweb.enquete.adapters.itemview;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ittechweb.enquete.R;
import com.ittechweb.enquete.models.Sondage;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by BM.SERVICES on 04/07/2017.
 */
@EViewGroup(R.layout.item_sondage)
public class SondageItemView extends LinearLayout {

    public SondageItemView(Context context) {
        super(context);
    }

    @ViewById(R.id.tv_libelle)
    TextView tvLibelle;

    @ViewById(R.id.tv_groupe)
    TextView tvGroupe;

    public void bind(Sondage sondage){
        tvLibelle.setText(sondage.getLibelle());
        tvGroupe.setText(sondage.getCategorie().getLibelle());
    }
}
