package com.ittechweb.enquete.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ittechweb.enquete.adapters.itemview.QuestionItemView;
import com.ittechweb.enquete.adapters.itemview.QuestionItemView_;
import com.ittechweb.enquete.adapters.itemview.SondageItemView;
import com.ittechweb.enquete.adapters.itemview.SondageItemView_;
import com.ittechweb.enquete.models.Question;
import com.ittechweb.enquete.models.Sondage;
import com.ittechweb.enquete.utils.Utils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

/**
 * Created by BM.SERVICES on 04/07/2017.
 */
@EBean
public class QuestionAdapter extends BaseAdapter {

    List<Question> questions;

    @RootContext
    Context context;

    public int idSondage;

    @AfterInject
    void initAdapter(){
        //this.notifyDataSetChanged();
    }

    public void setQuestions(int idSondage) {
        this.idSondage = idSondage;
        questions = Question.findBySondage(idSondage);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return questions.size();
    }

    @Override
    public Question getItem(int position) {
        return questions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        QuestionItemView questionItemView;
        if(convertView == null){
            questionItemView = QuestionItemView_.build(context);
        }
        else{
            questionItemView = (QuestionItemView) convertView;
        }

        questionItemView.bind(getItem(position));

        return questionItemView;
    }
}
