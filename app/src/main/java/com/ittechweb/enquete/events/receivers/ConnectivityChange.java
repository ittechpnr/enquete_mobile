package com.ittechweb.enquete.events.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ittechweb.enquete.services.TrackingService;

/**
 * Created by BM.SERVICES on 10/08/2018.
 */

public class ConnectivityChange extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent trackingIntent = new Intent(context, TrackingService.class);
        context.startService(trackingIntent);
    }
}
