package com.ittechweb.enquete.events;

/**
 * Created by wilfried on 18/04/2017.
 */

public class DoSynchroEvent {
    public boolean upload = false;

    public String jsonClient;

    public DoSynchroEvent(boolean upload) {
        this.upload = upload;
    }

    public DoSynchroEvent(boolean upload, String jsonClient) {
        this.upload = upload;
        this.jsonClient = jsonClient;
    }
}
