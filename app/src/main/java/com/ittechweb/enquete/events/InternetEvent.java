package com.ittechweb.enquete.events;

/**
 * Created by wilfried on 18/04/2017.
 */

public class InternetEvent {
    private boolean connected;
    private String msg;

    public InternetEvent() {
    }

    public InternetEvent(boolean connected, String msg) {
        this.connected = connected;
        this.msg = msg;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
