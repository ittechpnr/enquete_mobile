package com.ittechweb.enquete.events.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.events.InternetEvent;
import com.ittechweb.enquete.utils.OttoBus;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EReceiver;

/**
 * Created by wilfried on 18/04/2017.
 */
@EReceiver
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Bean
    OttoBus ottoBus;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(MainApplication.isNetworkConnected()){
            ottoBus.post(new InternetEvent(true, "Connexion disponible"));
        }
        else{
            ottoBus.post(new InternetEvent(false, "Connexion indisponible"));
        }
    }
}
