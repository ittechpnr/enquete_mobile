package com.ittechweb.enquete.events;

/**
 * Created by wilfried on 18/04/2017.
 */

public class SynchroEvent {
    String table;
    String message;
    int nombre;
    int restant;

    public SynchroEvent(String table, String message) {
        this.table = table;
        this.message = message;
    }

    public SynchroEvent(String table, String message, int restant) {
        this.table = table;
        this.message = message;
        this.restant = restant;
    }

    public SynchroEvent(String table, String message, int nombre, int restant) {
        this.table = table;
        this.message = message;
        this.nombre = nombre;
        this.restant = restant;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    public int getRestant() {
        return restant;
    }

    public void setRestant(int restant) {
        this.restant = restant;
    }
}
