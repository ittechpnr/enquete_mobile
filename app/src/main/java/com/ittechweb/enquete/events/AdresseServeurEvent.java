package com.ittechweb.enquete.events;

/**
 * Created by BM.SERVICES on 25/05/2017.
 */

public class AdresseServeurEvent {
    long code;
    String message;

    public AdresseServeurEvent(long code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
