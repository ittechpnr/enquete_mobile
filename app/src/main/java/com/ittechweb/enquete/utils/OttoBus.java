package com.ittechweb.enquete.utils;

import com.squareup.otto.Bus;

import org.androidannotations.annotations.EBean;

/**
 * Created by wilfried on 18/04/2017.
 */
@EBean(scope = EBean.Scope.Singleton)
public class OttoBus extends Bus {
}
