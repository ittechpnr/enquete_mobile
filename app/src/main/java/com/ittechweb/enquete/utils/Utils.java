package com.ittechweb.enquete.utils;

import android.app.Activity;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ittechweb.enquete.MainApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Wilfried on 16/05/2015.
 */
public class Utils {
    public static String TAG = "AppEnquete";

    public static void toast(String msg){
        Toast.makeText(MainApplication.getInstance().getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    public static Date stringToDate(String dateString, String format) {
        try {
            return  new SimpleDateFormat(format).parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String dateToString(Date date, String format) {
        try {
            return  new SimpleDateFormat(format).format(date);
        } catch (Exception ex){
            return "";
        }
    }

    public static <T> T jsonStringToObject(String json, Class<T>  tClass){
        try {
            Gson gson = new Gson();
            return gson.fromJson(json, tClass);
        }
        catch (Exception ex){
            Log.e(Config.TAG, "Convertion JSON : " + ex.getMessage());
            return null;
        }
    }

    public static String getCurrentDate(String pattern){
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        return dateTimeFormat.format(calendar.getTime());
    }

    public static boolean isNumeric(String string){
        String regexStr = "\\d+";

        if(string.matches(regexStr))
        {
            return true;
        }
        return false;
    }

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isAlphaNumeric(String string) {
        String regexStr = "[a-zA-Z0-9_-]*";

        if(string.matches(regexStr))
        {
            return true;
        }
        return false;
    }

    public static String toJsonString(Object obj){
        //Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").serializeNulls().excludeFieldsWithoutExposeAnnotation().create();
        Gson gson = new GsonBuilder().create();
        return gson.toJson(obj);
    }

    public static void hideKeybord(Activity mActivity){
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }
}
