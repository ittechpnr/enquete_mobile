package com.ittechweb.enquete.utils;

import java.util.List;

/**
 * Created by wilfried on 04/01/2016.
 */
public class GenericObjectResult<T> {

    private long code;

    private long total;

    private T data;

    private List<T> rows;

    private String message;

    private String msg;

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public String getMessage() {
        return msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
