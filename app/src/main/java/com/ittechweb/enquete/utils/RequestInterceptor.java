package com.ittechweb.enquete.utils;

import android.util.Log;

import com.ittechweb.enquete.MainApplication;
import com.ittechweb.enquete.models.User;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by BM.SERVICES on 28/06/2017.
 */

public class RequestInterceptor implements Interceptor {
    private Session<User> userSession;
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest;
        int idUser = 0;
        try {
            userSession = new Session<>(MainApplication.getInstance(), User.class);
            if(userSession.getFirst() != null){
                idUser = userSession.getFirst().getId();
            }
        }
        catch (Exception ex){
            Log.e(Config.TAG, "Erreur: " + ex.getMessage());
        }

        newRequest = request.newBuilder()
                .addHeader("ittoken", MainApplication.TOKEN)
                .addHeader("ftid", String.valueOf(idUser))
                .build();
        return chain.proceed(newRequest);
    }
}
