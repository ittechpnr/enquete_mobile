package com.ittechweb.enquete.utils;

/**
 * Created by Wilfried on 14/10/2015.
 */
public class Config {

    public static final String MODE = "DEV"; //DEV | PROD
    public static final String TAG = "APP_ENQUETE";
    public static final int niveauAlertSms = 3; //

    public static final String CONTENT_APPLICATION_JSON = "application/json";
    public static final String CONTENT_APPLICATION_STANDARD = "application/x-www-form-urlencoded";

    //GOOGLE PLAY SERVICES
    public static final String PROPERTY_APP_VERSION = "1.0.0";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final int UPDATE_INTERVAL = 1000*60*2; // 2 min
    public static final int FATEST_INTERVAL = 5000; // 5 sec
    public static final int DISPLACEMENT = 500; // 500 meters

    public static final String DATE_PATTERN = "dd-MM-yyyy";
    public static final String TIME_PATTERN = "HH:mm:ss";
    public static final String DATE_TIME_PATTERN = "dd-MM-yyyy HH:mm:ss";

    public static String getImei() {
        return "56497633344";
    }
}
