package com.ittechweb.enquete.models;

/**
 * Created by BM.SERVICES on 29/06/2018.
 */

public class Synchronisation {

    int sondageRequest;

    int sondageResponse;

    int questionRequest;

    int questionResponse;

    public Synchronisation() {
        this.sondageRequest = 0;
        this.sondageResponse = 0;
        this.questionRequest = 0;
        this.questionResponse = 0;
    }

    public int getSondageRequest() {
        return sondageRequest;
    }

    public void setSondageRequest(int sondageRequest) {
        this.sondageRequest = sondageRequest;
    }

    public void incrementSondage() {
        this.sondageRequest++;
    }

    public int getSondageResponse() {
        return sondageResponse;
    }

    public void setSondageResponse(int sondageResponse) {
        this.sondageResponse = sondageResponse;
    }

    public int getQuestionRequest() {
        return questionRequest;
    }

    public void incrementQuestion() {
        this.questionRequest++;
    }

    public void setQuestionRequest(int questionRequest) {
        this.questionRequest = questionRequest;
    }

    public int getQuestionResponse() {
        return questionResponse;
    }

    public void setQuestionResponse(int questionResponse) {
        this.questionResponse = questionResponse;
    }
}
