package com.ittechweb.enquete.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by BM.SERVICES on 23/05/2017.
 */

@Table(database = AppDatabase.class, insertConflict = ConflictAction.REPLACE, updateConflict = ConflictAction.REPLACE)
public class TypeQuestion extends BaseModel {

    @PrimaryKey(autoincrement = false)
    int id;

    @Column
    boolean etat;

    @Column
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    private String libelle;

    @Column
    private String type;

    public TypeQuestion() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TypeQuestion{" +
                "id=" + id +
                ", etat=" + etat +
                ", libelle='" + libelle + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public static List<TypeQuestion> findAll() {
        return SQLite.select().from(TypeQuestion.class).queryList();
    }

    public static List<TypeQuestion> findAll(int limit, int offset) {
        return SQLite.select().from(TypeQuestion.class).limit(limit).offset(offset).queryList();
    }
}
