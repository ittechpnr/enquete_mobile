package com.ittechweb.enquete.models;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by wilfried on 16/04/2017.
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase {

    public static final String NAME = "EnqueteDBTest"; //DB test, EnqueteDB2 pour test egalement
    //public static final String NAME = "EnqueteDB"; //Bonne DB

    public static final int VERSION = 1;
}
