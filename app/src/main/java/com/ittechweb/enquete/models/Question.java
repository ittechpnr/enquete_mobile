package com.ittechweb.enquete.models;

import com.google.gson.annotations.Expose;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BM.SERVICES on 23/05/2017.
 */

@Table(database = AppDatabase.class, insertConflict = ConflictAction.REPLACE, updateConflict = ConflictAction.REPLACE)
public class Question extends BaseModel {

    @PrimaryKey(autoincrement = false)
    int id;

    @Column
    boolean etat;

    @Column
    private String libelle;

    @Column
    private String lien;

    @ForeignKey(saveForeignKeyModel = false)
    @Expose(serialize = false)
    private Sondage sondage;

    @ForeignKey(saveForeignKeyModel = false)
    private TypeQuestion typeQuestion;

    @ForeignKey(saveForeignKeyModel = false)
    @Expose(serialize = false)
    private Question question = null;

    @ForeignKey(saveForeignKeyModel = false)
    @Expose(serialize = false)
    private Proposition proposition = null;

    @Expose(serialize = false)
    public List<Proposition> propositions = new ArrayList<>();

    public Question() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public Sondage getSondage() {
        return sondage;
    }

    public void setSondage(Sondage sondage) {
        this.sondage = sondage;
    }

    public TypeQuestion getTypeQuestion() {
        return typeQuestion;
    }

    public void setTypeQuestion(TypeQuestion typeQuestion) {
        this.typeQuestion = typeQuestion;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Proposition getProposition() {
        return proposition;
    }

    public void setProposition(Proposition proposition) {
        this.proposition = proposition;
    }

    public List<Proposition> getPropositions() {
        return propositions;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", etat=" + etat +
                ", libelle='" + libelle + '\'' +
                ", lien='" + lien + '\'' +
                ", sondage=" + sondage +
                ", typeQuestion=" + typeQuestion +
                ", question=" + question +
                ", proposition=" + proposition +
                ", propositions=" + propositions +
                '}';
    }

    public static List<Question> findAll() {
        return SQLite.select().from(Question.class).queryList();
    }

    public static List<Question> findAll(int limit, int offset) {
        return SQLite.select().from(Question.class).limit(limit).offset(offset).queryList();
    }

    public static List<Question> findBySondage(int idSondage) {
        return SQLite.select().from(Question.class).where(Question_Table.sondage_id.eq(idSondage)).queryList();
    }

    public static Question getById(int id) {
        return SQLite.select().from(Question.class).where(Question_Table.id.eq(id)).querySingle();
    }
}
