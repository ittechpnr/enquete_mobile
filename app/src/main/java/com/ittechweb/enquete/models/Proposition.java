package com.ittechweb.enquete.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by BM.SERVICES on 23/05/2017.
 */

@Table(database = AppDatabase.class, insertConflict = ConflictAction.REPLACE, updateConflict = ConflictAction.REPLACE)
public class Proposition extends BaseModel {

    @PrimaryKey(autoincrement = false)
    int id;

    @Column
    boolean etat;

    //Pour les propositions de type image, ce champs represente le lien de l'image
    @Column
    private String libelle;

    @Column
    private int min = 0;

    @Column
    private int max = 0;

    @Column
    private int step = 0;

    //Pour l'évaluation graduée, possibilité de choisir un intervalle
    @Column
    private boolean choixmultiple = false;

    @ForeignKey(saveForeignKeyModel = false)
    private Question question;

    @ForeignKey(saveForeignKeyModel = false)
    private TypeProposition typeProposition = null;

    public Proposition() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public TypeProposition getTypeProposition() {
        return typeProposition;
    }

    public void setTypeProposition(TypeProposition typeProposition) {
        this.typeProposition = typeProposition;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public boolean isChoixmultiple() {
        return choixmultiple;
    }

    public void setChoixmultiple(boolean choixmultiple) {
        this.choixmultiple = choixmultiple;
    }

    @Override
    public String toString() {
        return "Proposition{" +
                "id=" + id +
                ", etat=" + etat +
                ", libelle='" + libelle + '\'' +
                ", min=" + min +
                ", max=" + max +
                ", step=" + step +
                ", choixmultiple=" + choixmultiple +
                ", question=" + question +
                ", typeProposition=" + typeProposition +
                '}';
    }

    public static List<Proposition> findAll() {
        return SQLite.select().from(Proposition.class).queryList();
    }

    public static List<Proposition> findAll(int limit, int offset) {
        return SQLite.select().from(Proposition.class).limit(limit).offset(offset).queryList();
    }

    public static List<Proposition> findByQuestion(int idQuestion) {
        return SQLite.select().from(Proposition.class).where(Proposition_Table.question_id.eq(idQuestion)).queryList();
    }

    public static Proposition getById(int idPropositionLigne) {
        return SQLite.select().from(Proposition.class).where(Proposition_Table.id.eq(idPropositionLigne)).querySingle();
    }
}
