package com.ittechweb.enquete.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by BM.SERVICES on 23/05/2017.
 */

@Table(database = AppDatabase.class, insertConflict = ConflictAction.REPLACE, updateConflict = ConflictAction.REPLACE)
public class Equipement extends BaseModel {

    @PrimaryKey(autoincrement = false)
    int id;

    @Column
    boolean etat;

    @Column
    private String libelle;

    @Column
    private String imei;

    @Column
    private float latitude;

    @Column
    private float longitude;

    @Column
    private float batterie;

    @Column
    private boolean eteint;

    public Equipement() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getBatterie() {
        return batterie;
    }

    public void setBatterie(float batterie) {
        this.batterie = batterie;
    }

    public boolean isEteint() {
        return eteint;
    }

    public void setEteint(boolean eteint) {
        this.eteint = eteint;
    }

    @Override
    public String toString() {
        return "Equipement{" +
                "id=" + id +
                ", etat=" + etat +
                ", libelle='" + libelle + '\'' +
                ", imei='" + imei + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", batterie=" + batterie +
                ", eteint=" + eteint +
                '}';
    }
}
