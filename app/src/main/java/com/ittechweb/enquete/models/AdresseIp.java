package com.ittechweb.enquete.models;

/**
 * Created by BM.SERVICES on 11/02/2018.
 */

public class AdresseIp {
    private String adresse;

    public AdresseIp(String adresse) {
        this.adresse = adresse;
    }

    public String getAdresse() {
        return adresse;
    }
}
