package com.ittechweb.enquete.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by BM.SERVICES on 28/07/2017.
 */

@Table(database = AppDatabase.class, insertConflict = ConflictAction.REPLACE, updateConflict = ConflictAction.REPLACE)
public class TypeProposition extends BaseModel {

    @PrimaryKey(autoincrement = false)
    int id;

    @Column
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    private String libelle;

    @Column
    private String type = "";

    public TypeProposition() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TypeProposition{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public static List<TypeProposition> findAll() {
        return SQLite.select().from(TypeProposition.class).queryList();
    }

    public static List<TypeProposition> findAll(int limit, int offset) {
        return SQLite.select().from(TypeProposition.class).limit(limit).offset(offset).queryList();
    }
}
