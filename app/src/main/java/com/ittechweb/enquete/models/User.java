package com.ittechweb.enquete.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wilfried on 16/08/2016.
 */
@Table(database = AppDatabase.class, insertConflict = ConflictAction.REPLACE, updateConflict = ConflictAction.REPLACE)
public class User extends BaseModel {

    @PrimaryKey(autoincrement = false)
    int id;

    @Column
    private String contact;

    @Column
    private String email;

    @Column
    private String nom;

    @ForeignKey(saveForeignKeyModel = false)
    private Role role;

    List<Sondage> sondages = new ArrayList<>();

    public User() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Sondage> getSondages() {
        return sondages;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", contact='" + contact + '\'' +
                ", email='" + email + '\'' +
                ", nom='" + nom + '\'' +
                ", role=" + role +
                '}';
    }
}
