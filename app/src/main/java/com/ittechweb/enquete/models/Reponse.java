package com.ittechweb.enquete.models;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by BM.SERVICES on 24/05/2017.
 */
@Table(database = AppDatabase.class, insertConflict = ConflictAction.REPLACE, updateConflict = ConflictAction.REPLACE)
public class Reponse extends BaseModel {

    @PrimaryKey(autoincrement = true)
    int id;

    @Column
    boolean etat;

    @Column
    private String libre;

    @Column
    private int colonneIndex = 0;

    @Column
    @SerializedName("mini")
    private int min = 0;

    @Column
    @SerializedName("maxi")
    private int max = 0;

    @ForeignKey(saveForeignKeyModel = false)
    private Participant participant = null;

    //Pour les question de type matrice, ce champs represente la ligne de la matrine
    @ForeignKey(saveForeignKeyModel = false)
    private Proposition proposition = null;

    //Pour les questions de type matrice
    @ForeignKey(saveForeignKeyModel = false)
    private Proposition colonne = null;

    public Reponse() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEtat() {
        return etat;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public String getLibre() {
        return libre;
    }

    public void setLibre(String libre) {
        this.libre = libre;
    }

    public int getColonneIndex() {
        return colonneIndex;
    }

    public void setColonneIndex(int colonneIndex) {
        this.colonneIndex = colonneIndex;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public Proposition getProposition() {
        return proposition;
    }

    public void setProposition(Proposition proposition) {
        this.proposition = proposition;
    }

    public Proposition getColonne() {
        return colonne;
    }

    public void setColonne(Proposition colonne) {
        this.colonne = colonne;
    }

    @Override
    public String toString() {
        return "Reponse{" +
                "id=" + id +
                ", etat=" + etat +
                ", libre='" + libre + '\'' +
                ", colonneIndex=" + colonneIndex +
                ", min=" + min +
                ", max=" + max +
                ", participant=" + participant +
                ", proposition=" + proposition +
                ", colonne=" + colonne +
                '}';
    }

    public static List<Reponse> findAll() {
        return SQLite.select().from(Reponse.class).queryList();
    }

    public static List<Reponse> findByParticipant(int idParticipant) {
        return SQLite.select().from(Reponse.class).where(Reponse_Table.participant_id.eq(idParticipant)).queryList();
    }
}
