package com.ittechweb.enquete.models;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BM.SERVICES on 23/05/2017.
 */

@Table(database = AppDatabase.class, insertConflict = ConflictAction.REPLACE, updateConflict = ConflictAction.REPLACE)
public class Participant extends BaseModel {

    @SerializedName("idLocal")
    @PrimaryKey(autoincrement = true)
    int id;

    @Column
//    Unique(unique = true)
    @SerializedName("id")
    private int idRemote;

    @Column
    private String nom;

    @Column
    private int age;

    @Column
    private String contact;

    @Column
    private String sexe;

    @Column
    private String email;

    private List<Reponse> reponses = new ArrayList<>();

    public Participant() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Reponse> getReponses() {
        return reponses;
    }

    public void setReponses() {
        this.reponses = SQLite.select().from(Reponse.class).where(Reponse_Table.participant_id.eq(this.id)).limit(50).queryList();
    }

    public void setReponses(List<Reponse> reponses) {
        this.reponses = reponses;
    }

    public int getIdRemote() {
        return idRemote;
    }

    public void setIdRemote(int idRemote) {
        this.idRemote = idRemote;
    }

    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                ", idRemote=" + idRemote +
                ", nom='" + nom + '\'' +
                ", age=" + age +
                ", contact='" + contact + '\'' +
                ", sexe='" + sexe + '\'' +
                ", email='" + email + '\'' +
                ", reponses=" + reponses +
                '}';
    }

    public static List<Participant> findAll() {
        return SQLite.select().from(Participant.class).queryList();
    }

    public static List<Participant> findAllLocal() {
        return SQLite.select().from(Participant.class).where(Participant_Table.idRemote.eq(0)).queryList();
    }

    public static Participant getById(int participantId) {
        return SQLite.select().from(Participant.class).where(Participant_Table.id.eq(participantId)).querySingle();
    }
    public static Participant getOne(int offset) {
        return SQLite.select().from(Participant.class).where(Participant_Table.idRemote.eq(0)).offset(offset).querySingle();
    }
}
