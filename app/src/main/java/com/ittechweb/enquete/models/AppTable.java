package com.ittechweb.enquete.models;

import com.raizlabs.android.dbflow.sql.language.Delete;

/**
 * Created by wilfried on 16/04/2017.
 */

public class AppTable {
    public static void deleteAll() {
        //@TODO
        Delete.table(Reponse.class);
        Delete.table(Proposition.class);
        Delete.table(TypeProposition.class);
        Delete.table(Question.class);
        Delete.table(TypeProposition.class);
        Delete.table(Sondage.class);
        Delete.table(Categorie.class);
        Delete.table(Participant.class);
        Delete.table(User.class);
    }
}
